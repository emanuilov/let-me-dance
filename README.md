# Overview
Development of

# Prerequisites
1. [NPM](https://nodejs.org/en/) & `npm install -g yarn`
2. `npm install -g create-rx-app cross-env`
3. (Optional) Android SDK
4. (Optional) Windows 10 SDK
5. (Optional) iOS SDK

# Emulators
1. Android
   - If you would like to use the `yarn emulator:android` command you need to have previously created an AVD with name Nexus

# Cleaning
1. Android - `yarn clean:android`
2. iOS - `?`